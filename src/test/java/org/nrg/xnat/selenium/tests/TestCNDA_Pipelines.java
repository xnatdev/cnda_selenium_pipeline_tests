package org.nrg.xnat.selenium.tests;

import org.nrg.selenium.processing.BasePipelineTest;
import org.nrg.selenium.processing.configElements.*;
import org.nrg.selenium.util.AssortedUtils;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.PipelineCheckParams;
import org.nrg.testing.annotations.PipelineLaunchParams;
import org.nrg.testing.enums.Pipeline;
import org.nrg.testing.xnat.processing.CreatedAssessor;
import org.nrg.testing.xnat.processing.GeneratedEmail;
import org.nrg.testing.xnat.processing.files.ProcessingFileSetRequest;
import org.nrg.xnat.pogo.DataType;
import org.openqa.selenium.By;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

public class TestCNDA_Pipelines extends BasePipelineTest {

    private static final String FS_5_3_SESSION = "CNDA_E163604";
    private static final String PUP_MANUAL_SESSION = "CNDA_E164027";
    private static final String PUP_FS_SESSION = "CNDA_E169815";
    private static final String FBIRN_SESSION = "CNDA_E162947";
    private static final String ATLAS_SESSION = "CNDA_E166817";
    private static final String BOLD_SESSION = "CNDA_E166967";
    private static final String WMH_SESSION = "CNDA_E194256";
    private static final String ILP_SESSION = "CNDA_E185126";
    private static final DataType WMH = new DataType("wmh:wmhData", null, "WMH");
    private static final DataType PUP = new DataType("pup:pupTimeCourseData", null, "PUP Timecourse");

    private static By FULL_TABLE;

    @BeforeSuite
    public void checkAccount() {
        AssortedUtils.checkAccount(xnatDriver, mainCredentials(), "Selenium");
    }

    @BeforeSuite
    public void initTests() {
        FULL_TABLE = locators.FULL_SUMMARY_TABLE;
        DataType.QC.setSingularName("QC");
    }

    @Test
    @PipelineLaunchParams(estimatedRuntime = 14*60, session = FS_5_3_SESSION, pipeline = Pipeline.FS_5_3)
    @JiraKey(simpleKey = "CNDAQA-2")
    public void testLaunchFS_5_3() {
        config.add(new TestStepElement(By.name("FreesurferBuildOptions"), locators.BUTTON_ADVANCE));
        config.add(new TestStepElement(By.name("FreesurferBuildOptions"), locators.BUTTON_ADVANCE, By.xpath("//input[@value='4']")));
        config.add(BUTTON_ADVANCE_CLICK_ELEMENT);
        config.add(new TestStepElement(By.name("FreesurferBuildOptions"), locators.BUTTON_ADVANCE));
        config.add(new ClickElement(locators.OK_CLOSE_BUTTON));
        config.add(new TestStepElement(By.xpath("//div[contains(text(),'pipeline successfully queued.')]"), By.name("FreesurferBuildOptions"), locators.BUTTON_ADVANCE));
        config.add(new SimpleClickElement(locators.OK_CLOSE_BUTTON));
        performPipelineTest();
    }

    @Test
    @PipelineLaunchParams(estimatedRuntime = 120, session = PUP_MANUAL_SESSION, pipeline = Pipeline.PUP)
    @JiraKey(simpleKey = "CNDAQA-3")
    public void testLaunchManual_PUP() {
        config.add(new TestStepElement(By.id("start"), locators.BUTTON_ADVANCE));
        config.add(new SelectElement(By.name("pet_scan_id"), "4: DIAN PIB 336mtx (AC)"));
        config.add(new TestStepElement(By.id("start"), locators.BUTTON_ADVANCE));
        config.add(new ClickElement(By.xpath("//input[@value='customroi']")));
        config.add(new TestStepElement(By.id("start"), locators.BUTTON_ADVANCE));
        config.add(BUTTON_ADVANCE_CLICK_ELEMENT);
        config.add(new TestStepElement(By.id("path2"), locators.BUTTON_ADVANCE, By.xpath("//div[@id='path2' and @style='display: block;']")));
        config.add(new ClickElement(By.xpath("//input[@data-atltarg='711-2B']")));
        config.add(new TestStepElement(By.id("path2"), locators.BUTTON_ADVANCE));
        config.add(BUTTON_ADVANCE_CLICK_ELEMENT);
        config.add(new TestStepElement(By.id("path2-confirm"), locators.BUTTON_ADVANCE));
        config.add(BUTTON_ADVANCE_CLICK_ELEMENT);
        config.add(new TestStepElement(By.id("global-params"), locators.BUTTON_ADVANCE));
        config.add(new FillElement(By.name("refroistr"), "cerebellum"));
        config.add(new TestStepElement(By.id("global-params"), locators.BUTTON_ADVANCE));
        config.add(BUTTON_ADVANCE_CLICK_ELEMENT);
        config.add(new TestStepElement(By.id("confirm-params"), locators.BUTTON_ADVANCE, By.xpath("//h2[text()='Preview Pipeline Parameters for PUP_Manual_Session']")));
        config.add(new SimpleClickElement(locators.BUTTON_ADVANCE)); // setting timeout in exceptionSafeClick() blows up the alert
        config.add(new AlertElement("Build pipeline with these parameters?"));
        config.add(new TestStepElement(By.xpath("//*[contains(text(), 'The build process was successfully launched')]"), By.xpath("//input[@value='CLOSE']")));
        config.add(new SimpleClickElement(By.xpath("//input[@value='CLOSE']")));
        performPipelineTest();
    }

    @Test
    @PipelineLaunchParams(estimatedRuntime = 150, session = PUP_FS_SESSION, pipeline = Pipeline.PUP)
    @JiraKey(simpleKey = "CNDAQA-4")
    public void testLaunchFS_PUP() {
        config.add(new TestStepElement(By.id("start"), locators.BUTTON_ADVANCE));
        config.add(new SelectElement(By.name("pet_scan_id"), "5: DIAN PIB 336mtx (AC)"));
        config.add(new TestStepElement(By.id("start"), locators.BUTTON_ADVANCE));
        config.add(new ClickElement(By.xpath("//input[@value='freesurfer']")));
        config.add(new TestStepElement(By.id("start"), locators.BUTTON_ADVANCE));
        config.add(BUTTON_ADVANCE_CLICK_ELEMENT);
        config.add(new TestStepElement(By.id("path1"), locators.BUTTON_ADVANCE));
        config.add(new ClickElement(By.xpath("//input[@data-mrlabel='PUP_FS_MR']")));
        config.add(new TestStepElement(By.id("path1"), locators.BUTTON_ADVANCE));
        config.add(BUTTON_ADVANCE_CLICK_ELEMENT);
        config.add(new TestStepElement(By.id("global-params"), locators.BUTTON_ADVANCE));
        config.add(new FillElement(By.name("refroistr"), "Cerebellum-Cortex"));
        config.add(new TestStepElement(By.id("global-params"), locators.BUTTON_ADVANCE));
        config.add(BUTTON_ADVANCE_CLICK_ELEMENT);
        config.add(new TestStepElement(By.id("confirm-params"), locators.BUTTON_ADVANCE));
        config.add(new SimpleClickElement(locators.BUTTON_ADVANCE));
        config.add(new AlertElement("Build pipeline with these parameters?"));
        config.add(new TestStepElement(By.xpath("//*[contains(text(), 'The build process was successfully launched')]"), By.xpath("//input[@value='CLOSE']")));
        config.add(new SimpleClickElement(By.xpath("//input[@value='CLOSE']")));
        performPipelineTest();
    }

    @Test
    @PipelineLaunchParams(estimatedRuntime = 15, session = FBIRN_SESSION, pipeline = Pipeline.FBIRN)
    @JiraKey(simpleKey = "CNDAQA-6")
    public void testLaunchFBIRN() {
        config.add(new TestStepElement(By.name("GenericBoldBuildOptions")));
        config.add(new ClickElement(By.id("submitBtn")));
        config.add(new AssertTextElement(locators.MESSAGE, "The build process was successfully launched. Status email will be sent upon its completion."));
        config.add(new TestStepElement(locators.MESSAGE, By.xpath("//input[@type='Button']")));
        config.add(new SimpleClickElement(By.xpath("//input[@type='Button']")));
        performPipelineTest();
    }

    @Test
    @PipelineLaunchParams(estimatedRuntime = 90, session = BOLD_SESSION, pipeline = Pipeline.BOLD)
    @JiraKey(simpleKey = "CNDAQA-5")
    public void testLaunchBOLD() {
        final By RUN_LABEL = By.id("run_label");
        final By SESSION_ELT = By.xpath("//font[contains(text(),'Session: ')]");
        final By FINAL_RUN_LABEL = By.id("boldrunlabel_BOLD_4");
        final By[] PARAMS = new By[]{RUN_LABEL, SESSION_ELT, FINAL_RUN_LABEL};

        config.add(new TestStepElement(PARAMS));
        config.add(new ClickElement(RUN_LABEL));
        config.add(new TestStepElement(PARAMS));
        for (int scan : new int[]{7, 9, 11}) {
            config.add(new ClickElement(By.xpath(String.format("//input[@value='%d' and contains(@name, 'functional')]", scan))));
        }
        config.add(new TestStepElement(PARAMS));
        config.add(new SimpleClickElement(By.id("submitBtn")));
        config.add(new AlertElement(null));
        config.add(new TestStepElement(By.xpath("//b[text()='The build process was successfully launched. Status email will be sent upon its completion.']"), By.xpath("//input[@value='CLOSE']")));
        config.add(new SimpleClickElement(By.xpath("//input[@value='CLOSE']")));
        performPipelineTest();
    }

    @Test
    @PipelineLaunchParams(estimatedRuntime = 10, session = ATLAS_SESSION, pipeline = Pipeline.MR_TO_ATLAS)
    @JiraKey(simpleKey = "CNDAQA-7")
    public void testLaunchAtlas() {
        config.add(new TestStepElement(By.name("RegisterMR")));
        config.add(new SimpleClickElement(locators.BUTTON_ADVANCE));
        config.add(new AlertElement("Build pipeline with these parameters?"));
        config.add(new TestStepElement(By.xpath("//b[text()='The build process was successfully launched. Status email will be sent upon its completion.']"), By.xpath("//input[@value='CLOSE']")));
        config.add(new SimpleClickElement(By.xpath("//input[@value='CLOSE']")));
        performPipelineTest();
    }

    @Test
    @PipelineLaunchParams(estimatedRuntime = 20, session = WMH_SESSION, pipeline = Pipeline.WMH)
    @JiraKey(simpleKey = "CNDAQA-20")
    public void testLaunchWMH() {
        config.add(new TestStepElement(locators.FORM1_NAME));
        config.add(new ClickElement(By.id("param[0][1].value"))); // t1 scan = 4
        config.add(new ClickElement(By.id("param[2][0].value"))); // flair scan = 3
        config.add(new TestStepElement(locators.FORM1_NAME));
        config.add(new ClickElement(By.name("eventSubmit_doLaunchpipeline")));
        config.add(new TestStepElement(By.xpath("//b[contains(text(), 'The pipeline has been scheduled')]"), By.xpath("//input[@value='CLOSE']")));
        config.add(new SimpleClickElement(By.xpath("//input[@value='CLOSE']")));
        performPipelineTest();
    }

    /*@Test
    @PipelineLaunchParams(estimatedRuntime = ??????, session = ILP_SESSION, pipeline = Pipeline.ILP, customLaunch = true)
    public void testLaunchILP() {
        config.add(new ClickElement(By.linkText(Pipeline.ILP.getLaunchName())));
        config.add(new TestStepElement(locators.byText("ILP pipeline has been submitted to the queue.")));
        config.add(new ClickElement(locators.OK_BUTTON));
    }*/

    @Test
    @PipelineCheckParams(pipeline = Pipeline.FBIRN, maxRuntime = 1800, session = FBIRN_SESSION)
    @JiraKey(simpleKey = "CNDAQA-9")
    public void testCheckFBIRN() {
        final GeneratedEmail successEmail = new GeneratedEmail("fBIRN success email");
        successEmail.addSubjectCheck(String.format("CNDA update: Session %s Phantom QA complete", currentSession.getLabel()));
        successEmail.addMessageContainCriterion(String.format("Session %s has been processed.", currentSession.getLabel()));

        processingCheckables.add(new CreatedAssessor(DataType.QC));
        processingCheckables.add(new ProcessingFileSetRequest("FBIRN_files.yaml"));
        processingCheckables.add(successEmail);

        checkPipelineTest();
        xnatDriver.exceptionSafeClick(By.linkText(DataType.QC.getSingularName()));
        captureStep(locators.H2_TAG, By.xpath("//table[@bgcolor='white']"));

        compareTable(readElementsFile("FBIRN_values.yaml"), "fBIRN assessor values", locators.H2_TAG, By.xpath("//table[@bgcolor='white']"));
        logLogout();
        checkVerificationErrors();
    }

    @Test
    @PipelineCheckParams(pipeline = Pipeline.BOLD, maxRuntime = 3*3600, session = BOLD_SESSION)
    @JiraKey(simpleKey = "CNDAQA-8")
    public void testCheckBOLD() {
        final GeneratedEmail successEmail = new GeneratedEmail("BOLD success email");
        successEmail.addSubjectCheck(String.format("CNDA update: Session %s preprocessed", currentSession.getLabel()));
        successEmail.addMessageContainCriterion(String.format("4dfp BOLD Preprocessing is complete for Session %s.", currentSession.getLabel()));

        processingCheckables.add(new CreatedAssessor(DataType.QC));
        processingCheckables.add(new ProcessingFileSetRequest("BOLD_assessor_files.yaml"));
        processingCheckables.add(new ProcessingFileSetRequest("BOLD_resource_files.yaml"));
        processingCheckables.add(successEmail);

        checkPipelineTest();
        xnatDriver.exceptionSafeClick(By.linkText(DataType.QC.getSingularName()));
        captureStep(locators.H2_TAG, locators.DELETE_LINK);

        compareTable(readElementsFile("BOLD_assessor_values.yaml"), "BOLD assessor values", By.xpath("//table[3]"), By.xpath("//table[5]"));
        String assessorId = driver.findElement(By.xpath("//th[text()='Accession #:']/following-sibling::td")).getText();
        assertEquals(assessorId, driver.findElement(By.xpath("//table[5]/tbody/tr[3]/td[4]")).getText());
        captureScreenshotlessStep();
        logLogout();
        checkVerificationErrors();
    }

    @Test
    @PipelineCheckParams(pipeline = Pipeline.MR_TO_ATLAS, maxRuntime = 1800, session = ATLAS_SESSION)
    @JiraKey(simpleKey = "CNDAQA-10")
    public void testCheckAtlas() {
        final GeneratedEmail successEmail = new GeneratedEmail("Register MR to Atlas success email");
        successEmail.addSubjectCheck(String.format("CNDA update: %s registered to atlas", currentSession.getLabel()));
        successEmail.addMessageContainCriterion(String.format("RegisterMR has completed without errors for %s.", currentSession.getLabel()));

        processingCheckables.add(new ProcessingFileSetRequest("MR_To_Atlas_files.yaml"));
        processingCheckables.add(successEmail);

        checkPipelineTest();
        logLogout();
        checkVerificationErrors();
    }

    @Test
    @PipelineCheckParams(pipeline = Pipeline.WMH, maxRuntime = 3600, session = WMH_SESSION)
    @JiraKey(simpleKey = "CNDAQA-21")
    public void testCheckWMH() {
        final GeneratedEmail successEmail = new GeneratedEmail("WMH success email");
        successEmail.addSubjectCheck(String.format("CNDA update: %s processed with White Matter Hypointensities pipeline", currentSession.getLabel()));
        successEmail.addMessageContainCriterion(String.format("White Matter Hypointensities pipeline has completed without errors for %s.", currentSession.getLabel()));

        processingCheckables.add(new CreatedAssessor(WMH));
        processingCheckables.add(new ProcessingFileSetRequest("WMH_files.yaml"));
        processingCheckables.add(successEmail);

        checkPipelineTest();

        xnatDriver.exceptionSafeClick(By.linkText(WMH.getSingularName()));
        final By tbody = By.xpath("//*[@id='layout_content']/table/tbody");
        captureScreenshotlessStep();
        compareTable(readElementsFile("WMH_values.yaml"), "WMH values", new ByChained(tbody, By.xpath("./tr[1]")), new ByChained(tbody, By.xpath("./tr[2]")));
        logLogout();
        checkVerificationErrors();
    }

    @Test
    @PipelineCheckParams(pipeline = Pipeline.PUP, maxRuntime = 4*60*60, session = PUP_MANUAL_SESSION)
    @JiraKey(simpleKey = "CNDAQA-11")
    public void testCheckManual_PUP() {
        final GeneratedEmail successEmail = new GeneratedEmail("Manual PUP success email");
        successEmail.addSubjectCheck(String.format("CNDA update: %s processed with PUP", currentSession.getLabel()));
        successEmail.addMessageContainCriterion(String.format("PUP has completed without errors for %s.", currentSession.getLabel()));

        processingCheckables.add(new CreatedAssessor(PUP));
        processingCheckables.add(successEmail);

        checkPipelineTest();

        xnatDriver.exceptionSafeClick(By.linkText(PUP.getSingularName()));
        captureStep(locators.H2_TAG, locators.DELETE_LINK);

        compareTable(readElementsFile("PUP_Manual_values.yaml"), "PUP Manual Summary values",
                locators.SHORT_SUMMARY_TABLE, locators.QC_RESULTS_TABLE);

        driver.findElement(By.linkText("View Full Results")).click();
        xnatDriver.captureTable(FULL_TABLE, 16);

        compareTable(readElementsFile("PUP_Manual_values_page_2.yaml"), "PUP Manual Full values");

        logLogout();
        checkVerificationErrors();
    }

    @Test
    @PipelineCheckParams(pipeline = Pipeline.PUP, maxRuntime = 4*60*60, session = PUP_FS_SESSION)
    @JiraKey(simpleKey = "CNDAQA-12")
    public void testCheckFS_PUP() {
        final GeneratedEmail successEmail = new GeneratedEmail("FS PUP success email");
        successEmail.addSubjectCheck(String.format("CNDA update: %s processed with PUP", currentSession.getLabel()));
        successEmail.addMessageContainCriterion(String.format("PUP has completed without errors for %s.", currentSession.getLabel()));

        processingCheckables.add(new CreatedAssessor(PUP));
        processingCheckables.add(successEmail);

        checkPipelineTest();

        xnatDriver.exceptionSafeClick(By.linkText(PUP.getSingularName()));
        captureStep(locators.H2_TAG, locators.DELETE_LINK);

        compareTable(readElementsFile("PUP_FS_values.yaml"), "FS PUP Summary values",
                locators.SHORT_SUMMARY_TABLE, locators.QC_RESULTS_TABLE);

        driver.findElement(By.linkText("View Full Results")).click();
        xnatDriver.captureTable(FULL_TABLE, 16);

        compareTable(readElementsFile("PUP_FS_values_page_2.yaml"), "FS PUP Full values");

        logLogout();
        checkVerificationErrors();
    }

    @Test
    @PipelineCheckParams(pipeline = Pipeline.FS_5_3, maxRuntime = 16*60*60, session = FS_5_3_SESSION)
    @JiraKey(simpleKey = "CNDAQA-13")
    public void testCheckFS_5_3() {
        final GeneratedEmail successEmail = new GeneratedEmail("FS 5.3 success email");
        successEmail.addSubjectCheck(String.format("CNDA update: %s processed with Freesurfer 5.3", currentSession.getLabel()));
        successEmail.addMessageContainCriterion(String.format("Freesurfer 5.3 has completed without errors for %s.", currentSession.getLabel()));

        processingCheckables.add(new CreatedAssessor(DataType.FREESURFER));
        processingCheckables.add(new ProcessingFileSetRequest("FS_5_3_files.yaml"));
        processingCheckables.add(successEmail);

        checkPipelineTest();

        xnatDriver.exceptionSafeClick(By.linkText(DataType.FREESURFER.getSingularName()));
        captureStep(locators.H2_TAG, By.id("actionsMenu"));

        uploadTargetedScreenshot(By.xpath("//div[@id='analysis1']/table[1]"));
        xnatDriver.uploadTable(By.xpath("//div[@id='analysis1']/table[2]"), 16);
        compareTable(readElementsFile("FS_5_3_values.yaml"), "FS 5.3 page 1 values", locators.tab("Subcortical Segmentation"));

        driver.findElement(locators.tab("Surface Measures")).click();
        xnatDriver.uploadTable(By.xpath("//div[@id='analysis2']/table[1]"), 16);
        xnatDriver.captureTable(By.xpath("//div[@id='analysis2']/table[2]"), 16);
        compareTable(readElementsFile("FS_5_3_values_page_2.yaml"), "FS 5.3 page 2 values");

        logLogout();
        checkVerificationErrors();
    }

}