# This project launches and asserts that results (may be generated files, numerical values, or both) match expected results on an CNDA test server:#

* FreeSurfer 5.3
* PETUnified Pipeline (Manual)
* PETUnified Pipeline (FreeSurfer)
* BOLD Preprocessing
* MR to Atlas Registration
* fBIRN Phantom

# Admin Settings #

The xnat.requireAdmin setting should be set to false, as this project only uses a single non-admin account to be extra secure. If the account is not enabled correctly, a warning will be output.

# Need more Info? #

More information may be found in the README for the [nrg_selenium](http://www.bitbucket.org/xnatdev/nrg_selenium/) project